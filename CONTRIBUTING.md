# CONTRIBUTING

* Clone me
`git clone https://gitlab.com/yadevee/logo.git`

* Add new logos
* Build it
```
  docker build -t myname/logo-site:v1.0 .
```

* (Optionally) push it to repository
```
 docker push myname/logo-site:v1.0
```
* Commit changes
```
git commit -am "Message"
```
* Push changes
```
git push --all
```
